import React from "react";

const LoginLayout: React.FC = ({ children }) => {
  return (
    <div>
      <h2>Login Layout</h2>
      <div>{children}</div>
    </div>
  );
};

export default LoginLayout;
