import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { userLogout } from "../hooks/api/user";
import { RootState } from "../store";
import { logout } from "../store/authen";

const DefaultLayout: React.FC = ({ children }) => {
  const location = useLocation();
  const { user } = useSelector((state: RootState) => state.authen);
  const { role } = user;
  const dispatch = useDispatch();
  const logoutSubmit = async () => {
    await userLogout();
    dispatch(logout());
  };
  return (
    <div className="grid">
      <div
        className=" h-56 bg-center bg-cover bg-no-repeat"
        style={{
          backgroundImage: `url('http://www2.sskru.ac.th/2021/wp-content/uploads/2021/10/DSC02805-scaled.jpg')`,
          backgroundColor: `rgba(45,51,57,0.95)`,
        }}
      >
        <div
          className="w-full h-full text-white"
          style={{ backgroundColor: `rgba(45,51,57,0.5)` }}
        >
          <div className="container py-10">
            <h1
              className=" text-3xl text-yellow-500 font-bold"
              style={{ textShadow: `1px 1px #666666` }}
            >
              ระบบยื่นคำร้อง
            </h1>
            <b>มหาวิทยาลัยราชภัฏศรีสะเกษ</b>
          </div>
        </div>
      </div>
      <header className="flex items-center py-4">
        <div className="container text-sm">
          {role === 1 && (
            <>
              <Link
                className={`mr-3 ${
                  location.pathname === "/student" ? "font-bold" : ""
                }`}
                to="/student"
              >
                ติดตามสถานะ
              </Link>
              <Link
                className={`mr-3 ${
                  location.pathname === "/student/order" ? "font-bold" : ""
                }`}
                to="/student/order"
              >
                ยื่นคำร้อง
              </Link>
            </>
          )}
          {role === 2 && (
            <>
              <Link
                className={`mr-3 ${
                  location.pathname === "/involved" ? "font-bold" : ""
                }`}
                to="/involved"
              >
                ดำเนินการคำร้อง
              </Link>
            </>
          )}
          {role === 3 && (
            <>
              <Link
                className={`mr-3 ${
                  location.pathname.includes("/admin") &&
                  location.pathname.length <= 6
                    ? "font-bold"
                    : ""
                }`}
                to="/admin"
              >
                คำร้องที่ขอเข้ามา
              </Link>
              <Link
                className={`mr-3 ${
                  location.pathname.includes("/admin/petition")
                    ? "font-bold"
                    : ""
                }`}
                to="/admin/petition"
              >
                จัดการคำร้อง
              </Link>
              <Link
                className={`mr-3 ${
                  location.pathname === "/admin/settings" ? "font-bold" : ""
                }`}
                to="/admin/settings"
              >
                ตั้งค่าข้อมูล
              </Link>
            </>
          )}
          <button onClick={logoutSubmit}>ออกจากระบบ</button>
        </div>
      </header>
      <main className="container">
        <hr />
        <div className="min-h-screen pt-5 text-sm">{children}</div>
      </main>
    </div>
  );
};

export default DefaultLayout;
