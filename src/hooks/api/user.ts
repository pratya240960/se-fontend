import api from ".";
import { UserBody } from "./types/dataBody";
import { GetUserParam } from "./types/getParams";

export const userLogout = () => api.get(`/user/logout`);
export const getMe = () => api.get(`/user/me`);
export const getUserList = (param: GetUserParam) =>
  api.get(`/user?user=${param.user}&search=${param.search}&page=${param.page}`);
export const createUser = (data: UserBody[]) => api.post(`/user`, data);
export const updateUser = (data: UserBody[]) => api.put(`/user`, data);
export const deleteUser = (id: number) => api.delete(`/user?user=${id}`);
