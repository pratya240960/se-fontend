import api from ".";
import {
  PetitionBody,
  PetitionPriceBody,
  PetitionTypeBody,
} from "./types/dataBody";
import {
  GetPetitionParam,
  GetPetitionPriceParam,
  GetPetitionTypeParam,
} from "./types/getParams";

export const getPetitionList = (param: GetPetitionParam) =>
  api.get(
    `/petition?petition=${param.petition}&search=${param.search}&page=${param.page}`
  );
export const createPetition = (data: PetitionBody) =>
  api.post(`/petition`, {
    ...data,
    informations: data.informations?.map((i) => i.id),
  });
export const updatePetition = (data: PetitionBody, id: number) =>
  api.put(`/petition?petition=${id}`, {
    ...data,
    informations: data.informations?.map((i) => i.id),
  });
export const deletePetition = (id: number) =>
  api.delete(`/petition?petition=${id}`);

export const getPetitionTypeList = (param: GetPetitionTypeParam) =>
  api.get(
    `/petition/type?id=${param.id}&search=${param.search}&page=${param.page}`
  );
export const createPetitionType = (data: PetitionTypeBody) =>
  api.post(`/petition/type`, data);
export const updatePetitionType = (data: PetitionTypeBody, id: number) =>
  api.put(`/petition/type?id=${id}`, data);
export const deletePetitionType = (id: number) =>
  api.delete(`/petition/type?petition=${id}`);

export const getPetitionPriceList = (param: GetPetitionPriceParam) =>
  api.get(
    `/petition/price?id=${param.id}&search=${param.search}&page=${param.page}`
  );
export const createPetitionPrice = (data: PetitionPriceBody) =>
  api.post(`/petition/price`, data);
export const updatePetitionPrice = (data: PetitionPriceBody, id: number) =>
  api.put(`/petition/price?id=${id}`, data);
export const deletePetitionPrice = (id: number) =>
  api.delete(`/petition/price?petition=${id}`);
