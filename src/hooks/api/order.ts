import api from ".";
import { OrderBody, UpdateOrderStatusBody } from "./types/dataBody";
import { GetOrderParam } from "./types/getParams";

export const getOrderList = (param: GetOrderParam) =>
  api.get(
    `/order?order=${param.order}&search=${param.search}&page=${param.page}`
  );
export const createOrder = (data: OrderBody) => api.post(`/order`, data);
export const updateOrder = (data: OrderBody, id: number) =>
  api.put(`/order?id=${id}`, data);
export const updateOrderStatus = (data: UpdateOrderStatusBody, id: number) =>
  api.patch(`/order/update/status?order=${id}`, data);
export const deleteOrder = (id: number) => api.delete(`/order?order=${id}`);
