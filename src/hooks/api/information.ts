import api from ".";
import { InformationBody } from "./types/dataBody";
import { GetInformationParam } from "./types/getParams";

export const getInformationList = (param: GetInformationParam) =>
  api.get(
    `/information?information=${param.information}&search=${param.search}&page=${param.page}`
  );
export const createInformation = (data: InformationBody) =>
  api.post(`/information`, data);
export const updateInformation = (data: InformationBody, id: number) =>
  api.put(`/information?information=${id}`, data);
export const deleteInformation = (id: number) =>
  api.delete(`/information?information=${id}`);
