export type GetOrderParam = {
  order: number | string;
  search: string;
  page: number;
};
export type GetPetitionParam = {
  petition: number | string;
  search: string;
  page: number;
};
export type GetDepartmentParam = {
  department: number | string;
  search: string;
  page: number;
};
export type GetInformationParam = {
  information: number | string;
  search: string;
  page: number;
};
export type GetUserParam = {
  user: number | string;
  search: string;
  page: number;
};
export type GetPetitionTypeParam = {
  id: number | string;
  search: string;
  page: number;
};
export type GetPetitionPriceParam = {
  id: number | string;
  search: string;
  page: number;
};
