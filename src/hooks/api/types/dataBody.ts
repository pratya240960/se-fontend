export type UserBody = {
  id?: number;
  name?: string;
  surname?: string;
  email?: string;
  email_notification?: boolean;
  address?: string;
  username?: string;
  password?: string;
  role?: number;
  department?: DepartmentBody;
  created_at?: Date;
  time_now?: Date;
};
export type DepartmentBody = {
  id?: number;
  description?: string;
  keyword?: string;
  title?: string;
};
export type DepartmentOperateBody = {
  department?: number;
  isSuccess: boolean | null;
  number?: number;
  order?: number;
  title?: string;
};
export type PetitionTypeBody = {
  id?: number;
  title?: string;
};
export type PetitionPriceBody = {
  id?: number;
  price?: string;
  description?: string;
};
export type InformationBody = {
  id?: number;
  title?: string;
  type?: number;
  result?: any;
};
export type OrderBody = {
  id?: number;
  user?: UserBody;
  petition?: PetitionBody;
  quantity?: number;
  total?: number;
  delivery?: number;
  status?: number;
  description?: string;
  created_at?: Date;
  informations?: { id: number; result: any; title: string; type: number }[];
};
export type UpdateOrderStatusBody = {
  description?: string;
  approve?: boolean;
};
export type UserLevelBody = {
  id?: number;
  name?: number;
};
export type PetitionBody = {
  id?: number;
  type?: number;
  number?: number;
  title?: string;
  price?: number;
  user_level?: number;
  departments?: { id: number; order: number }[];
  informations?: { id: number; title: string; type: number }[];
  created_at?: Date;
};
