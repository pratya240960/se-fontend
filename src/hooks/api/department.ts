import api from ".";
import { DepartmentBody } from "./types/dataBody";
import { GetDepartmentParam } from "./types/getParams";

export const getDepartmentList = (param: GetDepartmentParam) =>
  api.get(
    `/department?department=${param.department}&search=${param.search}&page=${param.page}`
  );
export const createDepartment = (data: DepartmentBody) =>
  api.post(`/department`, data);
export const updateDepartment = (data: DepartmentBody, id: number) =>
  api.put(`/department?department=${id}`, data);
export const deleteDepartment = (id: number) =>
  api.delete(`/department?department=${id}`);
