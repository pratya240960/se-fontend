import api from ".";

export const uploadFile = (data: FormData, type: number, contentType: number) =>
  api.post(`/file?type=${type}&contentType=${contentType}`, data, {
    headers: {
      "content-type": "multipart/form-data",
    },
  });
