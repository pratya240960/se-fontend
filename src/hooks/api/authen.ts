import api from ".";
import { UserLogin } from "../../pages/Login/types";

export const login = (data: UserLogin) => api.post(`/authen/login`, data);
