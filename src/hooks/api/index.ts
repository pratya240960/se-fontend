import axios, { AxiosRequestHeaders } from "axios";
import configs from "../../configs";
import { showError } from "../sweetAlert";
import Cookies from "js-cookie";
const token = Cookies.get("accessToken") || "";
if (token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}
const headers: AxiosRequestHeaders = {
  "Content-type": "application/json",
  Accept: "application/json",
};

const api = axios.create({
  baseURL: configs.baseURL,
  withCredentials: true,
  headers,
});

let isRefreshing = false;
let failedQueue: {
  reject: any;
  resolve: any;
}[] = [];

const processQueue = (error?: any, token?: string) => {
  failedQueue.forEach((prom) => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });
  failedQueue = [];
};

api.interceptors.response.use(
  (config) => {
    return config;
  },
  async (error) => {
    const originalRequest = error.config;
    console.log(error);
    if (
      error.response.status === 401 &&
      originalRequest &&
      !originalRequest._isRetry
    ) {
      if (isRefreshing) {
        return new Promise(function (resolve, reject) {
          failedQueue.push({ resolve, reject });
        })
          .then(() => {
            return api.request(originalRequest);
          })
          .catch((error) => {
            throw error;
          });
      }
      originalRequest.isRetry = true;
      isRefreshing = true;
      try {
        await axios.get(`${configs.baseURL}/authen/refresh`, {
          withCredentials: true,
        });
        processQueue(null);
        isRefreshing = false;
        return api.request(originalRequest);
      } catch (err: any) {
        processQueue(err);
        showError(err.response?.data?.message);
        // setTimeout(() => {
        //   window.location.href = "/";
        // }, 2000);
        console.log(err.message);
      }
    }
    console.log(originalRequest);
    showError(error.response?.data?.message);
    throw error;
  }
);

export default api;
