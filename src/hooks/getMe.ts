import { useState, useEffect } from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import configs from "../configs";
import { setAuthen } from "../store/authen";
export function useLoadingWithRefresh() {
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.get(`${configs.baseURL}/user/me`, {
          withCredentials: true,
        });
        dispatch(setAuthen({ user: data.result }));
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    })();
  }, [dispatch]);

  return { loading };
}
