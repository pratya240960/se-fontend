import Swal from "sweetalert2";
const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

export const showConfirm = async (
  message = "คุณแน่ใจหรือไม่"
): Promise<boolean> => {
  const { isConfirmed } = await Swal.fire({
    title: message,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "ใช่",
  });
  return isConfirmed;
};
export const showSuccess = async (message = "ดำเนินการสำเร็จ") => {
  Toast.fire({
    icon: "success",
    title: message,
  });
};
export const showError = async (message = "ดำเนินการไม่สำเร็จ") => {
  Swal.fire({
    icon: "error",
    text: message,
  });
};
