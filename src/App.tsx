import React from "react";
import { Route, Routes } from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import LoginLayout from "./layouts/LoginLayout";
import LoginPage from "./pages/Login";
import { AdminRoute, InvolvedRoute, StudentRoute } from "./routes/privateRoute";
import { GuestRoute } from "./routes/publicRoute";
import StudentHomePage from "./pages/Student/Home";
import StudentOrderPage from "./pages/Student/Order";
import InvolvedHomePage from "./pages/Involved/Home";
import AdminHomePage from "./pages/Admin/Home";
import AdminPetitionPage from "./pages/Admin/Petition";
import AdminSettingPage from "./pages/Admin/Setting";
import { useLoadingWithRefresh } from "./hooks/getMe";
import { useSelector } from "react-redux";
import { RootState } from "./store";
import PetitionForm from "./pages/Admin/Petition/form";

const App: React.FC = () => {
  const { loading } = useLoadingWithRefresh();
  const { isAuth, user } = useSelector((state: RootState) => state.authen);
  return !loading ? (
    <>
      <Routes>
        <Route path="/" element={<GuestRoute />}>
          <Route index element={<LoginLayout children={<LoginPage />} />} />
        </Route>
        <Route path="student" element={<StudentRoute />}>
          <Route
            index
            element={<DefaultLayout children={<StudentHomePage />} />}
          />
          <Route
            path="order"
            element={<DefaultLayout children={<StudentOrderPage />} />}
          />
        </Route>
        <Route path="involved" element={<InvolvedRoute />}>
          <Route
            index
            element={<DefaultLayout children={<InvolvedHomePage />} />}
          />
        </Route>
        <Route path="admin" element={<AdminRoute />}>
          <Route
            index
            element={<DefaultLayout children={<AdminHomePage />} />}
          />
          <Route path="petition">
            <Route
              index
              element={<DefaultLayout children={<AdminPetitionPage />} />}
            />
            <Route
              path="create"
              element={<DefaultLayout children={<PetitionForm />} />}
            />
            <Route
              path=":id/update"
              element={<DefaultLayout children={<PetitionForm />} />}
            />
          </Route>
          <Route
            path="settings"
            element={<DefaultLayout children={<AdminSettingPage />} />}
          />
        </Route>
        <Route
          path="*"
          element={
            isAuth && user ? <DefaultLayout children={<>404</>} /> : <>404</>
          }
        />
      </Routes>
    </>
  ) : (
    <h1>Loading</h1>
  );
};
export default App;
