import { createSlice } from "@reduxjs/toolkit";
import { UserBody } from "../hooks/api/types/dataBody";
export type AuthenState = {
  isAuth: boolean;
  user: UserBody;
};
const initialState: AuthenState = {
  isAuth: false,
  user: {},
};

export const authenSlice = createSlice({
  name: "authen",
  initialState,
  reducers: {
    setAuthen: (
      state: AuthenState,
      action: { payload: { user: UserBody } }
    ) => {
      state.user = action.payload.user || {};
      state.isAuth = true;
    },
    logout: (state: AuthenState) => {
      state.user = {};
      state.isAuth = false;
    },
  },
});

export const { setAuthen, logout } = authenSlice.actions;

export default authenSlice.reducer;
