import { configureStore } from "@reduxjs/toolkit";
import authen from "./authen";

export const store = configureStore({
  reducer: {
    authen,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
