import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { RootState } from "../store";
export const GuestRoute: React.FC = () => {
  const { isAuth, user } = useSelector((state: RootState) => state.authen);
  if (!isAuth || !user.id) {
    return <Outlet />;
  } else {
    switch (user.role) {
      case 1:
        return <Navigate to="/student" />;
      case 2:
        return <Navigate to="/involved" />;
      case 3:
        return <Navigate to="/admin" />;
      default:
        return <Outlet />;
    }
  }
};
