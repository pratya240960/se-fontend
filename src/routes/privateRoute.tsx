import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { RootState } from "../store";
export const AdminRoute: React.FC = () => {
  const { isAuth, user } = useSelector((state: RootState) => state.authen);
  if (isAuth && user.id && user.role === 3) {
    return <Outlet />;
  } else {
    return <Navigate to="/" />;
  }
};
export const InvolvedRoute: React.FC = () => {
  const { isAuth, user } = useSelector((state: RootState) => state.authen);
  console.log({ isAuth, user });

  if (isAuth && user.id && user.role === 2) {
    return <Outlet />;
  } else {
    return <Navigate to="/" />;
  }
};
export const StudentRoute: React.FC = () => {
  const { isAuth, user } = useSelector((state: RootState) => state.authen);
  if (isAuth && user.id && user.role === 1) {
    return <Outlet />;
  } else {
    return <Navigate to="/" />;
  }
};
