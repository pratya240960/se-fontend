import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setAuthen } from "../../store/authen";
import useFormLogin from "./hooks";
import { UserLogin } from "./types";
import { login } from "../../hooks/api/authen";
const LoginPage: React.FC = () => {
  const { register, reset, handleSubmit, errors } = useFormLogin();
  const dispatch = useDispatch();
  const onSubmit = async (val: UserLogin): Promise<void> => {
    const { data } = await login(val);
    const { user } = data.result;
    dispatch(setAuthen({ user }));
  };
  useEffect(() => {
    return () => {
      reset();
    };
  }, [reset]);

  return (
    <div className="container">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="card p-10 grid gap-5 w-1/2 mx-auto">
          <b>Login</b>
          <div className="grid gap-3">
            <label htmlFor="username">รหัสนักศึกษา</label>
            <input
              {...register("username")}
              className="bg-gray-200 px-3 py-1"
              type="text"
              name="username"
              id="username"
            />
            <small className=" text-red-600">{errors.username?.message} </small>
          </div>
          <div className="grid gap-3">
            <label htmlFor="password">เลขประจำตัวประชาชน</label>
            <input
              {...register("password")}
              className="bg-gray-200 px-3 py-1"
              type="password"
              name="password"
              id="password"
            />
            <small className=" text-red-600">{errors.password?.message} </small>
          </div>
          <button type="submit" className=" bg-yellow-500 rounded-lg p-2">
            Login
          </button>
        </div>
      </form>
    </div>
  );
};

export default LoginPage;
