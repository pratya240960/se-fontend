import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { UserLogin } from "./types";

const validationSchema = Yup.object().shape({
  username: Yup.string()
    .required("Username is required")
    .min(4, "Username must be at least 6 characters")
    .max(20, "Username must not exceed 20 characters"),
  password: Yup.string()
    .required("Password is required")
    .min(4, "Password must be at least 6 characters")
    .max(40, "Password must not exceed 40 characters"),
});
const useFormLogin = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<UserLogin>({
    resolver: yupResolver(validationSchema),
  });
  return {
    register,
    handleSubmit,
    reset,
    errors,
  };
};

export default useFormLogin;
