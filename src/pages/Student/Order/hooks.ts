import { useEffect, useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import { OrderBody } from "../../../hooks/api/types/dataBody";
export const useFormOrder = () => {
  const [defaultValues, setDefaultValues] = useState<OrderBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
    setValue,
  } = useForm<OrderBody>({
    defaultValues,
  });

  const { fields } = useFieldArray({
    name: "informations",
    control,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    control,
    fields,
    setValue,
  };
};
