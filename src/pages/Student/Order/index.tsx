import React, { useEffect, useState } from "react";
import { Controller } from "react-hook-form";
import { getPetitionList } from "../../../hooks/api/petition";
import {
  OrderBody,
  PetitionBody,
  UserBody,
} from "../../../hooks/api/types/dataBody";
import {
  GetOrderParam,
  GetPetitionParam,
} from "../../../hooks/api/types/getParams";
import Select from "react-select";
import { useFormOrder } from "./hooks";
import { uploadFile } from "../../../hooks/api/file";
import configs from "../../../configs";
import { useFormUser } from "../../Admin/Setting/hooks";
import { useSelector } from "react-redux";
import { RootState } from "../../../store";
import { updateUser } from "../../../hooks/api/user";
import { showConfirm, showSuccess } from "../../../hooks/sweetAlert";
import {
  createOrder,
  deleteOrder,
  getOrderList,
  updateOrderStatus,
} from "../../../hooks/api/order";
import { OrderResponse } from "./types";
import { useNavigate } from "react-router-dom";

const FormStudentRequest: React.FC = () => {
  const [petitions, setPetitions] = useState<PetitionBody[]>([]);
  const [order, setOrder] = useState<OrderResponse | null>(null);
  const [reload, setReload] = useState(false);
  const {
    register,
    handleSubmit,
    reset,
    control,
    fields,
    setValue,
    setDefaultValues,
  } = useFormOrder();

  const getOrder = async (params: GetOrderParam) => {
    const { data } = await getOrderList(params);
    const result: OrderResponse | null =
      data.result?.filter((or: OrderResponse) => or.status === 1)[0] || null;

    if (result?.id) {
      const response = await getOrderList({ ...params, order: result.id });
      setOrder(response.data.result);
    } else {
      setOrder(null);
    }
    setReload(false);
  };

  const deleteClick = async (id: number) => {
    const isConfirm = await showConfirm();
    if (isConfirm) {
      await deleteOrder(id);
      showSuccess();
      setReload(true);
    }
  };

  const getPetition = async (param: GetPetitionParam) => {
    const { data } = await getPetitionList(param);
    if (param.petition) {
      setDefaultValues({
        informations: data.result.informations,
        petition: data.result.id,
      });
    } else {
      setPetitions(data.result);
    }
  };
  const onSubmit = async (val: OrderBody) => {
    console.log(val);
    val.status = 1;
    await createOrder(val);
    showSuccess();
    reset();
    setDefaultValues({});
    setReload(true);
  };

  useEffect(() => {
    setReload(true);
    getPetition({
      petition: "",
      page: 1,
      search: "",
    });
    return () => {
      reset();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reset]);

  useEffect(() => {
    if (reload) {
      getOrder({ order: "", page: 1, search: "" });
    }

    return () => {
      setReload(false);
    };
  }, [reload]);

  return (
    <div className="bg-white">
      <div id="card">
        <div className="mt-2">
          <div className="grid gap-3">
            <div className="grid">
              <UserData />
            </div>
            <div></div>
            <hr />
            <form onSubmit={handleSubmit(onSubmit)} className="grid gap-3">
              <div className="grid md:grid-cols-3">
                <div className="grid">
                  <label className=" font-bold" htmlFor="petition">
                    เลือกคำร้อง *
                  </label>
                  <Controller
                    control={control}
                    name="petition"
                    render={({ field: { value, onBlur, onChange } }) => {
                      console.log({ value });

                      return (
                        <Select
                          options={petitions}
                          getOptionLabel={(option: PetitionBody) =>
                            option.title ? option.title : ""
                          }
                          getOptionValue={(option) =>
                            option.id ? option.id.toString() : ""
                          }
                          onChange={(option) => {
                            onChange(option?.id);
                            if (option?.id) {
                              getPetition({
                                petition: option.id,
                                page: 1,
                                search: "",
                              });
                            }
                          }}
                          onBlur={onBlur}
                          className="w-full"
                          value={petitions.find((d) => d.id === value)}
                        />
                      );
                    }}
                  />
                </div>
              </div>
              <div className="grid">
                <div>
                  <small className="text-gray-500">ข้อมูลที่ต้องใช้ *</small>
                  <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-3 ml-2">
                    {fields.map((pi, i) => (
                      <div className="grid" key={pi.id}>
                        <input
                          type="hidden"
                          {...register(`informations.${i}.id`)}
                          value={pi.id}
                        />
                        <input
                          type="hidden"
                          {...register(`informations.${i}.type`)}
                          value={pi.type}
                        />
                        <label className=" font-bold" htmlFor="informations">
                          {pi.title}
                        </label>
                        {pi.type === 1 ? (
                          <input
                            type="text"
                            className="border border-gray-300 rounded p-2"
                            required
                            {...register(`informations.${i}.result`)}
                          />
                        ) : (
                          <input
                            type="file"
                            className="border border-gray-300 rounded p-2"
                            required
                            accept=".pdf"
                            onChange={async (e) => {
                              if (e.target.files) {
                                const attachFile = e.target.files[0];
                                const formData = new FormData();
                                formData.append("attachFile", attachFile);
                                const { data } = await uploadFile(
                                  formData,
                                  configs.petitionType,
                                  configs.orderContentType
                                );
                                if (data.result.id) {
                                  setValue(
                                    `informations.${i}.result`,
                                    data.result.id
                                  );
                                }
                              }
                            }}
                          />
                        )}
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="grid md:grid-cols-12">
                <div className="grid">
                  <label className=" font-bold" htmlFor="quantity">
                    จำนวนคำร้อง *
                  </label>
                  <input
                    type="number"
                    id="quantity"
                    className="border border-gray-300 rounded p-2"
                    defaultValue={1}
                    min={1}
                    {...register("quantity")}
                    required
                  />
                </div>
              </div>
              <div className="grid">
                <div>
                  <small className="text-gray-500">ตัวเลือกในการจัดส่ง *</small>
                  <div className="grid grid-cols-2 md:grid-cols-8 gap-5 ml-2">
                    <label htmlFor="emailDelivery">
                      <input
                        {...register("delivery")}
                        type="radio"
                        id="emailDelivery"
                        value={1}
                        name="delivery"
                        // defaultChecked={order.delivery === 1}
                      />
                      อีเมลล์
                    </label>
                    <label htmlFor="homeDelivery">
                      <input
                        {...register("delivery")}
                        type="radio"
                        id="homeDelivery"
                        value={2}
                        name="delivery"
                      />
                      บ้าน
                    </label>
                  </div>
                </div>
              </div>
              <div className="text-right">
                {order ? (
                  <button
                    className="py-2 px-3 text-white bg-red-500 rounded"
                    type="button"
                    onClick={() => deleteClick(order.id)}
                  >
                    ลบ
                  </button>
                ) : (
                  <button
                    className="py-2 px-3 text-white bg-yellow-500 rounded"
                    type="submit"
                  >
                    เพิ่ม
                  </button>
                )}
              </div>
            </form>
          </div>
        </div>
      </div>
      {order && (
        <div id="card" className=" border p-5 mt-3 grid grid-cols-2">
          <TablePreRequest order={order} />
          {order.price > 0 && (
            <div className="text-center">
              <b>ข้อมูลการชำระเงิน</b>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const TablePreRequest: React.FC<{
  order: OrderResponse;
}> = ({ order }: { order: OrderResponse }) => {
  const navigate = useNavigate();
  const updateOrderClick = async () => {
    await updateOrderStatus({ description: "", approve: true }, order.id);
    showSuccess();
    navigate("/student");
  };
  return (
    <div className="pl-1">
      {order && (
        <>
          <div>
            <b>คำร้องที่คุณเลือกไว้</b>
          </div>
          <div className="py-3"></div>
          <div>
            <b>หมายเลขติดตาม: </b>
            <span>{order.id}</span>
          </div>
          <div>
            <b>คำร้อง: </b>
            <span>{order.title}</span>
          </div>
          <div>
            <b>ข้อมูล: </b>
            <div className="pl-2">
              {order.informations.map((oi) => (
                <div key={oi.id}>
                  <b>{oi.title} :</b>
                  {oi.type === 1 ? (
                    <span> {oi.result}</span>
                  ) : (
                    <a
                      href={oi.result}
                      className=" text-blue-500 hover:underline"
                    >
                      {" "}
                      คลิกเพื่อดู
                    </a>
                  )}
                </div>
              ))}
            </div>
          </div>
          <div>
            <b>จำนวน: </b>
            <span>{order.quantity}</span>
          </div>
          <div>
            <b>ค่าธรรมเนียม: </b>
            <span>{order.price * order.quantity}</span>
          </div>
          <div>
            <b>ตัวเลือกในการจัดส่ง: </b>
            <span>{order.delivery === 1 ? "อีเมล" : "บ้าน"}</span>
          </div>
          <div className="py-3"></div>
          {order.price > 0 ? (
            <button
              className="py-2 px-3 text-white bg-yellow-500 rounded"
              type="button"
            >
              การชำระเงิน
            </button>
          ) : (
            <button
              className="py-2 px-3 text-white bg-yellow-500 rounded"
              type="button"
              onClick={updateOrderClick}
            >
              ยืนยัน
            </button>
          )}
        </>
      )}
    </div>
  );
};

export default FormStudentRequest;
const UserData: React.FC = () => {
  const { register, handleSubmit, reset, errors, setDefaultValues } =
    useFormUser();

  const { user } = useSelector((state: RootState) => state.authen);
  const onSubmit = async (val: UserBody) => {
    val.id = user.id;
    await updateUser([val]);
    showSuccess();
  };
  useEffect(() => {
    setDefaultValues(user);
    return () => {
      reset();
    };
  }, [reset, setDefaultValues, user]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="flex justify-between">
        <small className="text-gray-500">ข้อมูลติดต่อ *</small>
        <button className="text-green-500 ml-auto">บันทึก</button>
      </div>
      <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-3 ml-2">
        <div className="grid">
          <label className=" font-bold" htmlFor="email">
            อีเมลล์
          </label>
          <input
            type="email"
            id="email"
            className="border border-gray-300 rounded p-2"
            {...register("email")}
          />
          <small className=" text-red-500 font-bold">
            {errors.email?.message}{" "}
          </small>
        </div>
        {/* <div className="grid">
          <label className=" font-bold" htmlFor="phone">
            หมายเลขโทรศัพท์
          </label>
          <input
            type="text"
            name="phone"
            id="phone"
            className="border border-gray-300 rounded p-2"
          />
        </div> */}
        <div className="grid col-span-full md:col-span-2">
          <label className=" font-bold" htmlFor="address">
            ที่อยู่
          </label>
          <textarea
            id="address"
            rows={3}
            className="border border-gray-300 rounded p-2"
            {...register("address")}
          ></textarea>
          <small className=" text-red-500 font-bold">
            {errors.address?.message}
          </small>
        </div>
      </div>
    </form>
  );
};
