import {
  DepartmentBody,
  DepartmentOperateBody,
  InformationBody,
} from "../../../hooks/api/types/dataBody";

export type OrderResponse = {
  id: number;
  petition: number;
  title: string;
  description: string | null;
  delivery: number;
  quantity: number;
  price: number;
  actor_id: number;
  actor_name: string;
  status: number;
  informations: InformationBody[];
  departments: DepartmentBody[];
  department_operator: DepartmentOperateBody[];
  created_at: Date;
  updated_at: Date;
};
