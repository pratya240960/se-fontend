import React, { useEffect, useState } from "react";
import Dialog from "../../../components/modal";
import { GetStatusOrder, OrderContentDialog } from "../../../components/Order";
import { getOrderList } from "../../../hooks/api/order";
import { GetOrderParam } from "../../../hooks/api/types/getParams";
import { OrderResponse } from "../Order/types";

const StudentHomePage: React.FC = () => {
  const [orders, setOrders] = useState<OrderResponse[]>([]);
  const [order, setOrder] = useState<OrderResponse | null>(null);
  const [open, setOpen] = useState(false);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const getOrders = async (param: GetOrderParam) => {
    const { data } = await getOrderList(param);
    if (data.result) {
      setOrders(data.result.filter((o: OrderResponse) => o.status > 1));
    }
  };
  const statusClick = async (status: number, orderId: number) => {
    console.log(status);
    const { data } = await getOrderList({
      order: orderId,
      page: 1,
      search: "",
    });
    setOrder(data.result);
    setOpen(true);
  };
  useEffect(() => {
    getOrders({
      order: "",
      page,
      search,
    });
    return () => {
      setOrders([]);
    };
  }, [page, search]);
  return (
    <div className="overflow-x-scroll">
      <small className=" text-red-500">*กดที่สถานะเพื่อตรวจสอบ/ดำเนินการ</small>
      <table
        className="w-full text-left border-collapse border"
        style={{
          minWidth: "600px",
        }}
      >
        <thead className="border-bottom py-10">
          <tr>
            <th className="border p-2">หมายเลขติดตาม</th>
            <th className="border p-2">คำร้อง</th>
            <th className="border p-2 text-center">วันที่ยื่นคำร้อง</th>
            <th className="border p-2 text-center">ดำเนินการล่าสุด</th>
            <th className="border p-2 text-center">สถานะ</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order.id} className="hover:bg-gray-100">
              <td className="border p-2">{order.id}</td>
              <td className="border p-2">{order.title} </td>
              <td className="border p-2">
                {new Date(order.created_at).toLocaleString("th-TH")}
              </td>
              <td className="border p-2">
                {new Date(order.updated_at).toLocaleString("th-TH")}
              </td>
              <td className="border p-2 text-center">
                <GetStatusOrder
                  status={order.status}
                  statusClick={(val) => statusClick(val, order.id)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <Dialog open={open} setOpen={setOpen}>
        {order ? (
          <OrderContentDialog order={order} onReload={() => {}} />
        ) : (
          <>Loading</>
        )}
      </Dialog>
    </div>
  );
};

export default StudentHomePage;
