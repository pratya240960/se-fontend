import React, { useEffect, useState } from "react";
import Dialog from "../../../components/modal";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../store";
import { useFormUser } from "../../Admin/Setting/hooks";
import { UserBody } from "../../../hooks/api/types/dataBody";
import { updateUser } from "../../../hooks/api/user";
import { showSuccess } from "../../../hooks/sweetAlert";
import { setAuthen } from "../../../store/authen";
import { OrderResponse } from "../../Student/Order/types";
import { GetOrderParam } from "../../../hooks/api/types/getParams";
import { getOrderList } from "../../../hooks/api/order";
import { OrderContentDialog } from "../../../components/Order";

const InvolvedHomePage: React.FC = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [typeContent, setTypeContent] = useState("email");
  const { user } = useSelector((state: RootState) => state.authen);
  const [order, setOrder] = useState(null);
  const [emailNoti, setEmailNoti] = useState<boolean | undefined>(
    user.email_notification
  );
  const dispatch = useDispatch();
  async function changeEmailNoti(e: React.ChangeEvent<HTMLInputElement>) {
    if (e.target.checked) {
      setTypeContent("email");
      setIsOpen(true);
      setEmailNoti(true);
    }
    const checked: boolean = !!user.email && e.target.checked;
    setEmailNoti(checked);
    await updateUser([{ ...user, email_notification: checked }]);
    dispatch(setAuthen({ user: { ...user, email_notification: checked } }));
  }

  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const [orders, setOrders] = useState<OrderResponse[]>([]);
  const getOrders = async (param: GetOrderParam) => {
    const { data } = await getOrderList(param);
    if (data.result) {
      setOrders(data.result.filter((o: OrderResponse) => o.status > 1));
    }
  };
  useEffect(() => {
    getOrders({
      order: "",
      page,
      search,
    });
    return () => {
      setOrders([]);
    };
  }, [page, search]);
  return (
    <div>
      <div className="flex justify-end items-center py-2">
        <input
          type="checkbox"
          name="email_notification"
          id="email_notification"
          className="mr-2"
          onChange={changeEmailNoti}
          checked={emailNoti}
        />
        <label htmlFor="email_notification">
          อนุญาติให้แจ้งเตือนผ่าน Email
        </label>
      </div>
      <table
        className="w-full text-left border-collapse border"
        style={{ minWidth: "900px" }}
      >
        <thead className="border-bottom py-10">
          <tr>
            <th className="border p-2">คำร้อง</th>
            <th className="border p-2">รหัสนักศึกษา</th>
            <th className="border p-2">ชื่อนักศึกษา</th>
            <th className="border p-2 text-right">จัดการ</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order.id} className="hover:bg-gray-100">
              <td className="border p-2">{order.title} </td>
              <td className="border p-2">{order.actor_id}</td>
              <td className="border p-2">{order.actor_name} </td>
              <td className="border p-2 text-right">
                <button
                  className="bg-yellow-500 text-white py-1 px-2 rounded"
                  onClick={async () => {
                    setTypeContent("check");
                    setIsOpen(true);
                    const { data } = await getOrderList({
                      order: order.id,
                      page: 1,
                      search: "",
                    });
                    setOrder(data.result);
                  }}
                >
                  ตรวจสอบ
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Dialog
        open={modalIsOpen}
        setOpen={(isOpen) => {
          setIsOpen(isOpen);
          if (!isOpen && !user.email) {
            setIsOpen(isOpen);
            setEmailNoti(false);
          }
        }}
        children={
          <IsContentDialog
            type={typeContent}
            setIsOpen={(isOpen) => {
              setIsOpen(isOpen);
              getOrders({
                order: "",
                page,
                search,
              });
            }}
            user={user}
            order={order}
          />
        }
      />
    </div>
  );
};

export default InvolvedHomePage;

export const IsContentDialog: React.FC<{
  type: string;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  user: UserBody;
  order: OrderResponse | null;
}> = ({ type, setIsOpen, user, order }) => {
  const { register, handleSubmit, setDefaultValues } = useFormUser();
  const dispatch = useDispatch();
  const emailSubmit = async (val: UserBody) => {
    val.id = user.id;
    val.email_notification = true;
    await updateUser([val]);
    dispatch(setAuthen({ user: val }));
    showSuccess();
    setIsOpen(false);
  };
  useEffect(() => {
    if (type === "email") {
      setDefaultValues(user);
    }
    return () => {};
  }, [setDefaultValues, type, user]);

  if (type === "email") {
    return (
      <>
        <form className=" grid" onSubmit={handleSubmit(emailSubmit)}>
          <label htmlFor="email">อีเมลล์</label>
          <input
            className="border border-gray-300 rounded p-2"
            type="email"
            id="email"
            required
            {...register("email")}
          />
          <button className=" bg-yellow-500 p-1 rounded-lg mt-3" type="submit">
            บันทึก
          </button>
        </form>
      </>
    );
  } else {
    return order ? (
      <OrderContentDialog order={order} onReload={() => setIsOpen(false)} />
    ) : (
      <>Loading</>
    );
  }
};
