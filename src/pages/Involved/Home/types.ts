export type OrderProps = {
  id: number;
  user: number;
  petition: number;
  quantity: number;
  total: number;
  delivery: number;
  status: number;
  description: string;
  created_at: Date;
  informations: object[];
  department_operator: object[];
};
