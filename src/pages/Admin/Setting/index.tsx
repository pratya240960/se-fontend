import React, { useEffect, useState } from "react";
import Dialog from "../../../components/modal";
import { IsContentDialog } from "../../Involved/Home";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import Select from "react-select";
import {
  createUser,
  deleteUser,
  getUserList,
  updateUser,
} from "../../../hooks/api/user";
import {
  GetDepartmentParam,
  GetInformationParam,
  GetPetitionPriceParam,
  GetPetitionTypeParam,
  GetUserParam,
} from "../../../hooks/api/types/getParams";
import {
  createDepartment,
  deleteDepartment,
  getDepartmentList,
  updateDepartment,
} from "../../../hooks/api/department";
import {
  createPetitionPrice,
  createPetitionType,
  deletePetitionPrice,
  deletePetitionType,
  getPetitionPriceList,
  getPetitionTypeList,
  updatePetitionPrice,
  updatePetitionType,
} from "../../../hooks/api/petition";
import { InformationProps } from "../Information/types";
import {
  createInformation,
  deleteInformation,
  getInformationList,
  updateInformation,
} from "../../../hooks/api/information";
import {
  useFormDepartment,
  useFormInformation,
  useFormPetitionPrice,
  useFormPetitionType,
  useFormUser,
} from "./hooks";
import {
  DepartmentBody,
  InformationBody,
  PetitionPriceBody,
  PetitionTypeBody,
  UserBody,
} from "../../../hooks/api/types/dataBody";
import { showConfirm, showSuccess } from "../../../hooks/sweetAlert";
import { Controller } from "react-hook-form";

const AdminSettingPage: React.FC = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [typeContent, setTypeContent] = useState("email");

  function changeEmailNoti(e: React.ChangeEvent<HTMLInputElement>) {
    if (e.target.defaultChecked) {
      setTypeContent("email");
      setIsOpen(true);
    }
  }
  return (
    <div className="">
      <div className="flex items-center py-2">
        <input
          type="checkbox"
          name="email_notification"
          id="email_notification"
          className="mr-2"
          onChange={changeEmailNoti}
        />
        <label htmlFor="email_notification">
          อนุญาติให้แจ้งเตือนผ่าน Email
        </label>
      </div>
      <div id="setting-request">
        <div className="grid">
          <small className="text-gray-600 pb-3">ตั้งค่าการจัดการคำร้อง</small>
          <div className="pl-2 grid gap-5">
            <DepartmentModule />
            <div>
              <hr />
            </div>
            <UserModule />
            <div>
              <hr />
            </div>
            <InformationModule />
            <div>
              <hr />
            </div>
            <PetitionTypeModule />
            <div>
              <hr />
            </div>
            <PetitionPriceModule />
            <div></div>
          </div>
        </div>
      </div>
      {/* <div id="author">
        <div className="grid pt-2">
          <small className="text-gray-600">อื่น ๆ</small>
          <form className="pl-2">
            <div className="grid">
              <textarea
                name="author"
                id="author"
                cols={30}
                rows={3}
                className="border border-gray-300 rounded p-2"
              ></textarea>
            </div>
            <button className="bg-yellow-500 text-white font-bold rounded px-3 py-1 my-2">
              บันทึก
            </button>
          </form>
        </div>
      </div> */}
      <Dialog
        open={modalIsOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <IsContentDialog
            type={typeContent}
            setIsOpen={(isOpen) => setIsOpen(isOpen)}
            user={{}}
            order={null}
          />
        }
      />
    </div>
  );
};

export default AdminSettingPage;

const DepartmentModule: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const eyeClick = () => {
    setIsShow(!isShow);
  };
  const [departments, setDepartments] = useState<DepartmentBody[]>([]);

  const getDepartment = async (param: GetDepartmentParam): Promise<void> => {
    const { data } = await getDepartmentList(param);
    setDepartments(data.result);
  };

  const {
    register,
    reset,
    handleSubmit,
    errors,
    setDefaultValues,
    defaultValues,
  } = useFormDepartment();
  const editClick = (id?: number) => {
    const findData = departments.find((d) => d.id === id);
    if (findData) {
      setDefaultValues(findData);
      setIsOpen(true);
    }
  };
  const deleteClick = async (id?: number): Promise<void> => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deleteDepartment(id);
      getDepartment({
        department: "",
        page: 1,
        search: "",
      });
      showSuccess();
    }
  };
  const onSubmit = async (val: DepartmentBody): Promise<void> => {
    let resp: any;
    if (defaultValues.id) {
      resp = await updateDepartment(val, defaultValues.id);
    } else {
      resp = await createDepartment(val);
    }
    console.log(resp?.data);
    showSuccess();
    setIsOpen(false);
  };

  useEffect(() => {
    if (isShow) {
      getDepartment({
        department: "",
        page: 1,
        search: "",
      });
    } else if (!isShow) {
      setDepartments([]);
    }
  }, [isShow]);

  useEffect(() => {
    if (!isOpen) {
      reset();
      setDefaultValues({});
      getDepartment({
        department: "",
        page: 1,
        search: "",
      });
    }
  }, [isOpen, reset, setDefaultValues]);

  return (
    <div>
      <div className="flex justify-between">
        <b className="text-gray-600 cursor-pointer" onClick={eyeClick}>
          หน่วยงาน * &nbsp;
          <FontAwesomeIcon
            className={isShow ? `text-yellow-500` : `text-gray-500`}
            icon={faEye}
          />
        </b>

        {isShow && (
          <div className="pl-2 pb-2">
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              placeholder="ค้นหา.."
            />
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              onClick={() => setIsOpen(true)}
            >
              เพิ่ม
            </button>
          </div>
        )}
      </div>
      {isShow && (
        <div>
          <table
            className="w-full text-left border-collapse border"
            style={{
              minWidth: "600px",
            }}
          >
            <thead className="border-bottom py-10">
              <tr>
                <th className="border p-2">#</th>
                <th className="border p-2">หน่วยงาน</th>
                <th className="border p-2">คีเวิร์ด</th>
                <th className="border p-2 w-32">จัดการ</th>
              </tr>
            </thead>
            <tbody>
              {departments.map((d) => (
                <tr className="hover:bg-gray-100" key={d.id}>
                  <td className="border p-2">{d.id}</td>
                  <td className="border p-2">{d.description}</td>
                  <td className="border p-2">{d.keyword}</td>
                  <td className="border p-2">
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => editClick(d.id)}
                    >
                      แก้ไข
                    </button>
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => deleteClick(d.id)}
                    >
                      ลบ
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}

      <Dialog
        open={isOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="grid mb-2">
              <b>หน่วยงาน</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("description")}
              />
              <small className="text-red-500 font-bold">
                {errors.description?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>Keyword</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("keyword")}
              />
              <small className="text-red-500 font-bold">
                {errors.keyword?.message}
              </small>
            </div>
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              type="submit"
            >
              เพิ่ม
            </button>
          </form>
        }
      />
    </div>
  );
};

const UserModule: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [users, setUsers] = useState<UserBody[]>([]);
  const [departments, setDepartments] = useState<DepartmentBody[]>([]);

  const eyeClick = () => {
    setIsShow(!isShow);
  };
  const getUsers = async (param: GetUserParam): Promise<void> => {
    const { data } = await getUserList(param);
    setUsers(data.result);
  };

  const {
    register,
    reset,
    handleSubmit,
    errors,
    setDefaultValues,
    defaultValues,
    control,
  } = useFormUser();
  const editClick = (id?: number) => {
    const findData = users.find((d) => d.id === id);
    if (findData) {
      setDefaultValues(findData);
      setIsOpen(true);
    }
  };
  const deleteClick = async (id?: number): Promise<void> => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deleteUser(id);
      getUsers({
        user: "",
        page: 1,
        search: "",
      });
      showSuccess();
    }
  };

  const onSubmit = async (val: UserBody): Promise<void> => {
    console.log(val);
    console.log("totpo");
    let resp: any;
    if (defaultValues.id) {
      resp = await updateUser([{ ...val, id: defaultValues.id }]);
    } else {
      resp = await createUser([val]);
    }
    console.log(resp?.data);
    showSuccess();
    setIsOpen(false);
  };

  const getDepartment = async (param: GetDepartmentParam): Promise<void> => {
    const { data } = await getDepartmentList(param);
    setDepartments(data.result);
  };

  useEffect(() => {
    if (isShow) {
      getUsers({
        user: "",
        page: 1,
        search: "",
      });
      getDepartment({
        department: "",
        page: 1,
        search: "",
      });
    } else {
      setUsers([]);
      setDepartments([]);
    }
    return () => {};
  }, [isShow]);

  useEffect(() => {
    if (!isOpen) {
      reset();
      setDefaultValues({});
      getDepartment({
        department: "",
        page: 1,
        search: "",
      });
      getUsers({
        user: "",
        page: 1,
        search: "",
      });
    }
  }, [isOpen, reset, setDefaultValues]);

  return (
    <div>
      <div className="flex justify-between">
        <b className="text-gray-600 cursor-pointer" onClick={eyeClick}>
          ผู้ใช้งานระบบ * &nbsp;
          <FontAwesomeIcon
            className={isShow ? `text-yellow-500` : `text-gray-500`}
            icon={faEye}
          />
        </b>
        {isShow && (
          <div className="pl-2 pb-2">
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              placeholder="ค้นหา.."
            />
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              onClick={() => setIsOpen(true)}
            >
              เพิ่ม
            </button>
          </div>
        )}
      </div>
      {isShow && (
        <div>
          <table
            className="w-full text-left border-collapse border"
            style={{
              minWidth: "600px",
            }}
          >
            <thead className="border-bottom py-10">
              <tr>
                <th className="border p-2">ชื่อสกุล</th>
                <th className="border p-2">อีเมลล์</th>
                <th className="border p-2">ที่อยู่</th>
                <th className="border p-2">ชื่อผู้ใช้</th>
                <th className="border p-2">รหัสผ่าน</th>
                <th className="border p-2">สถานะ</th>
                <th className="border p-2">หน่วยงาน</th>
                <th className="border p-2 w-32">จัดการ</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr className="hover:bg-gray-100" key={user.id}>
                  <td className="border p-2">
                    {user.name + " " + user.surname}
                  </td>
                  <td className="border p-2">{user.email}</td>
                  <td className="border p-2">{user.address}</td>
                  <td className="border p-2">{user.username}</td>
                  <td className="border p-2">{user.password}</td>
                  <td className="border p-2">{user.role}</td>
                  <td className="border p-2">{user.department}</td>
                  <td className="border p-2">
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => editClick(user.id)}
                    >
                      แก้ไข
                    </button>
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => deleteClick(user.id)}
                    >
                      ลบ
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}

      <Dialog
        open={isOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="grid mb-2">
              <b>ชื่อ</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("name")}
              />
              <small className="text-red-500 font-bold">
                {errors.name?.message}{" "}
              </small>
            </div>
            <div className="grid mb-2">
              <b>นามสกุล</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("surname")}
              />
              <small className="text-red-500 font-bold">
                {errors.surname?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>อีเมลล์</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("email")}
              />
              <small className="text-red-500 font-bold">
                {errors.email?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>ที่อยู่</b>
              <textarea
                id="address"
                cols={30}
                className="border border-gray-300 rounded p-2"
                {...register("address")}
              ></textarea>
              <small className="text-red-500 font-bold">
                {errors.address?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>ชื่อผู้ใช้</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("username")}
              />
              <small className="text-red-500 font-bold">
                {errors.username?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>รหัสผ่าน</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("password")}
              />
              <small className="text-red-500 font-bold">
                {errors.password?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>หน่วยงาน</b>
              <Controller
                control={control}
                name="department"
                render={({ field: { value, onBlur, onChange } }) => (
                  <Select
                    options={departments}
                    getOptionLabel={(option) =>
                      option.description ? option.description : ""
                    }
                    getOptionValue={(option) =>
                      option.id ? option.id.toString() : ""
                    }
                    onChange={(option) => onChange(option?.id)}
                    onBlur={onBlur}
                    value={departments.find((d) => d.id === value)}
                  />
                )}
              />
              <small className="text-red-500 font-bold">
                {errors.department?.id?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>สถานะ</b>
              <div className=" grid grid-cols-3">
                <label htmlFor="student">
                  <input
                    {...register("role")}
                    type="radio"
                    id="student"
                    value={1}
                    name="role"
                    defaultChecked={defaultValues.role === 1}
                  />
                  นักศึกษา
                </label>
                <label htmlFor="invollved">
                  <input
                    {...register("role")}
                    type="radio"
                    id="invollved"
                    value={2}
                    name="role"
                    defaultChecked={defaultValues.role === 2}
                  />
                  ผู้ที่เห็นชอบ
                </label>
                <label htmlFor="admin">
                  <input
                    {...register("role")}
                    type="radio"
                    id="admin"
                    value={3}
                    name="role"
                    defaultChecked={defaultValues.role === 3}
                  />
                  แอดทิน
                </label>
              </div>
              <small className="text-red-500 font-bold">
                {errors.role?.message}
              </small>
            </div>
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              type="submit"
            >
              {defaultValues.id ? "บันทึก" : "เพิ่ม"}
            </button>
          </form>
        }
      />
    </div>
  );
};

const PetitionPriceModule: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const eyeClick = () => {
    setIsShow(!isShow);
  };
  const [petitionPrices, setPetitionPrices] = useState<PetitionPriceBody[]>([]);

  const getPetitionPrice = async (
    param: GetPetitionPriceParam
  ): Promise<void> => {
    const { data } = await getPetitionPriceList(param);
    setPetitionPrices(data.result);
  };

  const {
    register,
    reset,
    handleSubmit,
    errors,
    setDefaultValues,
    defaultValues,
  } = useFormPetitionPrice();
  const editClick = (id?: number) => {
    const findData = petitionPrices.find((d) => d.id === id);
    if (findData) {
      setDefaultValues(findData);
      setIsOpen(true);
    }
  };
  const deleteClick = async (id?: number): Promise<void> => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deletePetitionPrice(id);
      getPetitionPrice({
        id: "",
        page: 1,
        search: "",
      });
      showSuccess();
    }
  };
  const onSubmit = async (val: PetitionPriceBody): Promise<void> => {
    let resp: any;
    if (defaultValues.id) {
      resp = await updatePetitionPrice(val, defaultValues.id);
    } else {
      resp = await createPetitionPrice(val);
    }
    console.log(resp?.data);
    showSuccess();
    setIsOpen(false);
  };
  useEffect(() => {
    if (isShow) {
      getPetitionPrice({
        id: "",
        page: 1,
        search: "",
      });
    } else {
      setPetitionPrices([]);
    }
  }, [isShow]);

  useEffect(() => {
    if (!isOpen) {
      reset();
      setDefaultValues({});
      getPetitionPrice({
        id: "",
        page: 1,
        search: "",
      });
    }
  }, [isOpen, reset, setDefaultValues]);

  return (
    <div>
      <div className="flex justify-between">
        <b className="text-gray-600 cursor-pointer" onClick={eyeClick}>
          ค่าธรรมเนียม * &nbsp;
          <FontAwesomeIcon
            className={isShow ? `text-yellow-500` : `text-gray-500`}
            icon={faEye}
          />
        </b>
        {isShow && (
          <div className="pl-2 pb-2">
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              placeholder="ค้นหา.."
            />
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              onClick={() => setIsOpen(true)}
            >
              เพิ่ม
            </button>
          </div>
        )}
      </div>
      {isShow && (
        <div>
          <table
            className="w-full text-left border-collapse border"
            style={{
              minWidth: "600px",
            }}
          >
            <thead className="border-bottom py-10">
              <tr>
                <th className="border p-2">#</th>
                <th className="border p-2">ค่าธรรมเนียม</th>
                <th className="border p-2">คำอธิบาย</th>
                <th className="border p-2 w-32">จัดการ</th>
              </tr>
            </thead>
            <tbody>
              {petitionPrices.map((pp) => (
                <tr className="hover:bg-gray-100" key={pp.id}>
                  <td className="border p-2">{pp.id}</td>
                  <td className="border p-2">{pp.price}</td>
                  <td className="border p-2">{pp.description}</td>
                  <td className="border p-2">
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => editClick(pp.id)}
                    >
                      แก้ไข
                    </button>
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => deleteClick(pp.id)}
                    >
                      ลบ
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}

      <Dialog
        open={isOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="grid mb-2">
              <b>ค่าธรรมเนียม</b>
              <input
                type="number"
                className="border border-gray-300 rounded p-2"
                {...register("price")}
              />
              <small className=" text-red-500 font-bold">
                {errors.price?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>คำอธิบาย</b>
              <textarea
                id="address"
                cols={30}
                className="border border-gray-300 rounded p-2"
                {...register("description")}
              ></textarea>
              <small className=" text-red-500 font-bold">
                {errors.description?.message}
              </small>
            </div>
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              type="submit"
            >
              เพิ่ม
            </button>
          </form>
        }
      />
    </div>
  );
};

const PetitionTypeModule: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const eyeClick = () => {
    setIsShow(!isShow);
  };
  const [petitionTypes, setPetitionTypes] = useState<PetitionTypeBody[]>([]);

  const getPetitionType = async (
    param: GetPetitionTypeParam
  ): Promise<void> => {
    const { data } = await getPetitionTypeList(param);
    setPetitionTypes(data.result);
  };

  const {
    register,
    reset,
    handleSubmit,
    errors,
    setDefaultValues,
    defaultValues,
  } = useFormPetitionType();
  const editClick = (id?: number) => {
    const findData = petitionTypes.find((d) => d.id === id);
    if (findData) {
      setDefaultValues(findData);
      setIsOpen(true);
    }
  };
  const deleteClick = async (id?: number): Promise<void> => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deletePetitionType(id);
      getPetitionType({
        id: "",
        page: 1,
        search: "",
      });
      showSuccess();
    }
  };
  const onSubmit = async (val: PetitionTypeBody): Promise<void> => {
    let resp: any;
    if (defaultValues.id) {
      resp = await updatePetitionType(val, defaultValues.id);
    } else {
      resp = await createPetitionType(val);
    }
    console.log(resp?.data);
    showSuccess();
    setIsOpen(false);
  };
  useEffect(() => {
    if (isShow) {
      getPetitionType({
        id: "",
        page: 1,
        search: "",
      });
    } else {
      setPetitionTypes([]);
    }
  }, [isShow]);

  useEffect(() => {
    if (!isOpen) {
      reset();
      setDefaultValues({});
      getPetitionType({
        id: "",
        page: 1,
        search: "",
      });
    }
  }, [isOpen, reset, setDefaultValues]);
  return (
    <div>
      <div className="flex justify-between">
        <b className="text-gray-600 cursor-pointer" onClick={eyeClick}>
          ประเภท/วัตถุประสงค์ * &nbsp;
          <FontAwesomeIcon
            className={isShow ? `text-yellow-500` : `text-gray-500`}
            icon={faEye}
          />
        </b>
        {isShow && (
          <div className="pl-2 pb-2">
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              placeholder="ค้นหา.."
            />
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              onClick={() => setIsOpen(true)}
            >
              เพิ่ม
            </button>
          </div>
        )}
      </div>
      {isShow && (
        <div>
          <table
            className="w-full text-left border-collapse border"
            style={{
              minWidth: "600px",
            }}
          >
            <thead className="border-bottom py-10">
              <tr>
                <th className="border p-2">#</th>
                <th className="border p-2">ประเภท/วัตถุประสงค์</th>
                <th className="border p-2 w-32">จัดการ</th>
              </tr>
            </thead>
            <tbody>
              {petitionTypes.map((pt) => (
                <tr className="hover:bg-gray-100" key={pt.id}>
                  <td className="border p-2">{pt.id}</td>
                  <td className="border p-2">{pt.title}</td>
                  <td className="border p-2">
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => editClick(pt.id)}
                    >
                      แก้ไข
                    </button>
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => deleteClick(pt.id)}
                    >
                      ลบ
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}

      <Dialog
        open={isOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="grid mb-2">
              <b>Title</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("title")}
              />
              <small className=" text-red-500 font-bold">
                {errors.title?.message}
              </small>
            </div>
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              type="submit"
            >
              เพิ่ม
            </button>
          </form>
        }
      />
    </div>
  );
};

const InformationModule: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const eyeClick = () => {
    setIsShow(!isShow);
  };
  const [infprmations, setInformations] = useState<InformationProps[]>([]);

  const getInformation = async (param: GetInformationParam): Promise<void> => {
    const { data } = await getInformationList(param);
    setInformations(data.result);
  };

  const {
    register,
    reset,
    handleSubmit,
    errors,
    setDefaultValues,
    defaultValues,
  } = useFormInformation();
  const editClick = (id?: number) => {
    const findData = infprmations.find((d) => d.id === id);
    if (findData) {
      setDefaultValues(findData);
      setIsOpen(true);
    }
  };
  const deleteClick = async (id?: number): Promise<void> => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deleteInformation(id);
      getInformation({
        information: "",
        page: 1,
        search: "",
      });
      showSuccess();
    }
  };
  const onSubmit = async (val: InformationBody): Promise<void> => {
    let resp: any;
    if (defaultValues.id) {
      resp = await updateInformation(val, defaultValues.id);
    } else {
      resp = await createInformation(val);
    }
    console.log(resp?.data);
    showSuccess();
    setIsOpen(false);
  };

  useEffect(() => {
    if (isShow) {
      getInformation({
        information: "",
        page: 1,
        search: "",
      });
    } else {
      setInformations([]);
    }
  }, [isShow]);

  useEffect(() => {
    if (!isOpen) {
      reset();
      setDefaultValues({});
      getInformation({
        information: "",
        page: 1,
        search: "",
      });
    }
  }, [isOpen, reset, setDefaultValues]);
  return (
    <div>
      <div className="flex justify-between">
        <b className="text-gray-600 cursor-pointer" onClick={eyeClick}>
          ข้อมูลของคำร้อง * &nbsp;
          <FontAwesomeIcon
            className={isShow ? `text-yellow-500` : `text-gray-500`}
            icon={faEye}
          />
        </b>
        {isShow && (
          <div className="pl-2 pb-2">
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              placeholder="ค้นหา.."
            />
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              onClick={() => setIsOpen(true)}
            >
              เพิ่ม
            </button>
          </div>
        )}
      </div>
      {isShow && (
        <div>
          <table
            className="w-full text-left border-collapse border"
            style={{
              minWidth: "600px",
            }}
          >
            <thead className="border-bottom py-10">
              <tr>
                <th className="border p-2">#</th>
                <th className="border p-2">ข้อมูล</th>
                <th className="border p-2">ประเภท</th>
                <th className="border p-2 w-32">จัดการ</th>
              </tr>
            </thead>
            <tbody>
              {infprmations.map((ift) => (
                <tr className="hover:bg-gray-100" key={ift.id}>
                  <td className="border p-2">{ift.id}</td>
                  <td className="border p-2">{ift.title}</td>
                  <td className="border p-2">{ift.type}</td>
                  <td className="border p-2">
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => editClick(ift.id)}
                    >
                      แก้ไข
                    </button>
                    <button
                      className=" p-2 rounded-lg px-3"
                      onClick={() => deleteClick(ift.id)}
                    >
                      ลบ
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}

      <Dialog
        open={isOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="grid mb-2">
              <b>Label</b>
              <input
                type="text"
                className="border border-gray-300 rounded p-2"
                {...register("title")}
              />
              <small className="text-red-500 font-bold">
                {errors.title?.message}
              </small>
            </div>
            <div className="grid mb-2">
              <b>ประเภท</b>
              <div className=" grid grid-cols-3">
                <label htmlFor="text">
                  <input
                    {...register("type")}
                    type="radio"
                    id="text"
                    value={1}
                    name="type"
                    defaultChecked={defaultValues.type === 1}
                  />
                  ข้อความ
                </label>
                <label htmlFor="file">
                  <input
                    {...register("type")}
                    type="radio"
                    id="file"
                    value={2}
                    name="type"
                    defaultChecked={defaultValues.type === 2}
                  />
                  ไฟล์
                </label>
              </div>
              <small className="text-red-500 font-bold">
                {errors.type?.message}
              </small>
            </div>
            <button
              className="p-2 rounded bg-yellow-500 text-white"
              type="submit"
            >
              เพิ่ม
            </button>
          </form>
        }
      />
    </div>
  );
};
