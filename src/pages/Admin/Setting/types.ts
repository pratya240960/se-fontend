export type PetitionProps = {
  id: number;
  type: number;
  number: number;
  title: number;
  price: number;
  user_level: number;
  created_at: Date;
};
