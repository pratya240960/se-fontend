import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  DepartmentBody,
  InformationBody,
  PetitionPriceBody,
  PetitionTypeBody,
  UserBody,
} from "../../../hooks/api/types/dataBody";
import { useEffect, useState } from "react";

export const useFormDepartment = () => {
  const validationSchema = Yup.object().shape({
    keyword: Yup.string()
      .required("Keyword is required")
      .max(10, "Keyword must not exceed 10 characters"),
  });
  const [defaultValues, setDefaultValues] = useState<DepartmentBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<DepartmentBody>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
  };
};
export const useFormUser = () => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required("Name is required")
      .min(4, "Name must be at least 4 characters")
      .max(100, "Name must not exceed 100 characters"),
    surname: Yup.string()
      .required("Surname is required")
      .min(4, "Surname must be at least 4 characters")
      .max(100, "Surname must not exceed 100 characters"),
    username: Yup.string()
      .required("Username is required")
      .min(4, "Username must be at least 4 characters")
      .max(100, "Username must not exceed 100 characters"),
    password: Yup.string()
      .required("Password is required")
      .min(4, "Password must be at least 4 characters")
      .max(100, "Password must not exceed 100 characters"),
    email: Yup.string().email("Email invalid"),
    role: Yup.number().required("Role is required"),
  });
  const [defaultValues, setDefaultValues] = useState<UserBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
  } = useForm<UserBody>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    control,
  };
};

export const useFormInformation = () => {
  const validationSchema = Yup.object().shape({
    title: Yup.string()
      .required("Label is required")
      .max(100, "Label must not exceed 100 characters"),
    type: Yup.number(),
  });
  const [defaultValues, setDefaultValues] = useState<InformationBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
  } = useForm<InformationBody>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    control,
  };
};
export const useFormPetitionType = () => {
  const validationSchema = Yup.object().shape({
    title: Yup.string()
      .required("Title is required")
      .max(100, "Title must not exceed 100 characters"),
  });
  const [defaultValues, setDefaultValues] = useState<PetitionTypeBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
  } = useForm<PetitionTypeBody>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    control,
  };
};
export const useFormPetitionPrice = () => {
  const validationSchema = Yup.object().shape({
    price: Yup.number().required("Price is required"),
    description: Yup.string().max(
      100,
      "Description must not exceed 100 characters"
    ),
  });
  const [defaultValues, setDefaultValues] = useState<PetitionPriceBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
  } = useForm<PetitionPriceBody>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    control,
  };
};
