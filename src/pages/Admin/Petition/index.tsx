import React, { useEffect, useState } from "react";
import Dialog from "../../../components/modal";
import { Step, Stepper } from "react-form-stepper";
import { GetPetitionParam } from "../../../hooks/api/types/getParams";
import { Link } from "react-router-dom";
import { deletePetition, getPetitionList } from "../../../hooks/api/petition";
import { PetitionBody } from "../../../hooks/api/types/dataBody";
import { showConfirm, showSuccess } from "../../../hooks/sweetAlert";

const AdminPetitionPage: React.FC = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [petitions, setPetitions] = useState<PetitionBody[]>([]);
  const [typeContent] = useState("email");
  const [petition, setPetition] = useState<PetitionBody>();
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const getPetition = async (param: GetPetitionParam) => {
    const { data } = await getPetitionList(param);
    if (data.result) {
      setPetitions(data.result);
    }
  };
  const deleteClick = async (id?: number) => {
    const isConfirmed = await showConfirm();
    if (isConfirmed && id) {
      await deletePetition(id);
      showSuccess();
      getPetition({
        petition: petition?.id || "",
        search,
        page,
      });
    }
  };
  useEffect(() => {
    getPetition({
      petition: petition?.id || "",
      search,
      page,
    });
    return () => {
      setPetitions([]);
    };
  }, [page, petition?.id, search]);
  return (
    <div className=" grid gap-3">
      <div>
        <Link
          className=" px-3 p-1 bg-blue-500 text-white rounded-lg"
          to={"/admin/petition/create"}
        >
          เพิ่ม
        </Link>
      </div>
      <table
        className="w-full text-left border-collapse border"
        style={{ minWidth: "900px" }}
      >
        <thead className="border-bottom py-10">
          <tr>
            <th className="border p-2">หมายเลขคำร้อง</th>
            <th className="border p-2">คำร้อง</th>
            <th className="border p-2">นักศึกษา</th>
            <th className="border p-2 text-right">จัดการ</th>
          </tr>
        </thead>
        <tbody>
          {petitions.map((petition) => (
            <tr key={petition.id} className="hover:bg-gray-100">
              <td className="border p-2">{petition.number} </td>
              <td className="border p-2">{petition.title}</td>
              <td className="border p-2">{petition.user_level} </td>
              <td className="border p-2 text-right">
                <Link
                  to={`/admin/petition/${petition.id}/update`}
                  className="bg-yellow-500 text-white py-1 px-2 rounded"
                >
                  แก้ไข
                </Link>
                <button
                  className="bg-red-500 text-white py-1 px-2 rounded"
                  onClick={() => deleteClick(petition.id)}
                >
                  ลบ
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Dialog
        open={modalIsOpen}
        setOpen={(isOpen) => setIsOpen(isOpen)}
        children={
          <IsContentDialog
            type={typeContent}
            setIsOpen={(isOpen) => setIsOpen(isOpen)}
          />
        }
      />
    </div>
  );
};

export default AdminPetitionPage;

const IsContentDialog: React.FC<{
  type: string;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}> = ({ type, setIsOpen }) => {
  if (type === "email") {
    return (
      <>
        <form className=" grid">
          <label htmlFor="email">อีเมลล์</label>
          <input
            className="border border-gray-300 rounded p-2"
            type="email"
            name="email"
            id="email"
          />
          <button className=" bg-yellow-500 p-1 rounded-lg mt-3">บันทึก</button>
        </form>
      </>
    );
  } else {
    return (
      <>
        <div className="text-center">
          <b className=" text-lg">คำร้อง 1</b>
        </div>
        <Stepper activeStep={2}>
          <Step label="อาจารย์ที่ปรึกษา" />
          <Step label="คณะกรรมการบริหารหลักสูตร" />
          <Step label="คณะบดี คณะศิลปศาสตร์และวิทยาศาสตร์" />
        </Stepper>
        <table className="text-left w-full">
          <tbody>
            <tr>
              <th className="py-1 w-32">วันที่ยื่นคำร้อง</th>
              <td>{new Date().toLocaleString()}</td>
            </tr>
            <tr>
              <th className="py-1 w-32">หมายเลขคำร้อง</th>
              <td>123</td>
            </tr>
            <tr>
              <th className="py-1 w-32">คำร้อง</th>
              <td>123</td>
            </tr>
            <tr>
              <th className="py-1 w-32">รายละเอียด</th>
              <td>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Maxime, odit!
              </td>
            </tr>
            <tr>
              <th className="py-1 w-32">นักศึกษา</th>
              <td>นายปรัชญา มณีโชติ</td>
            </tr>
            <tr>
              <td colSpan={2} className="pt-3">
                <div>
                  <b>ข้อคิดเห็น</b>
                  <textarea
                    name="memo"
                    className="w-full bg-gray-100 rounded"
                    rows={3}
                  ></textarea>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="flex items-center justify-end pt-5 rounded-b">
          <button
            className="px-3 py-1 rounded text-red-500"
            type="button"
            onClick={() => setIsOpen(false)}
          >
            ไม่เห็นชอบ
          </button>
          <button
            className="bg-yellow-600 px-3 py-1 rounded text-white"
            type="button"
            onClick={() => setIsOpen(false)}
          >
            เห็นชอบ
          </button>
        </div>
      </>
    );
  }
};
