import React, { useEffect, useState } from "react";
import { Controller } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import Select from "react-select";
import { getDepartmentList } from "../../../hooks/api/department";
import { getInformationList } from "../../../hooks/api/information";
import {
  createPetition,
  getPetitionList,
  getPetitionPriceList,
  getPetitionTypeList,
  updatePetition,
} from "../../../hooks/api/petition";
import {
  DepartmentBody,
  InformationBody,
  PetitionBody,
  PetitionPriceBody,
  PetitionTypeBody,
} from "../../../hooks/api/types/dataBody";
import { GetPetitionParam } from "../../../hooks/api/types/getParams";
import { showSuccess } from "../../../hooks/sweetAlert";
import { useFormPetition } from "./hooks";

enum GetData {
  "petition_type",
  "petition_price",
  "information",
  "department",
}

const PetitionForm: React.FC = () => {
  const [informations, setInformations] = useState<InformationBody[]>([]);
  const [departments, setDepartments] = useState<DepartmentBody[]>([]);
  const [petitionTypes, setPetitionTypes] = useState<PetitionTypeBody[]>([]);
  const [petitionPrices, setPetitionPrices] = useState<PetitionPriceBody[]>([]);
  const navigate = useNavigate();
  const params = useParams();
  const getData = async (type: GetData): Promise<void> => {
    let response: any;
    switch (type) {
      case GetData.petition_type:
        response = await getPetitionTypeList({ id: "", page: 1, search: "" });
        setPetitionTypes(response.data.result);
        break;
      case GetData.petition_price:
        response = await getPetitionPriceList({ id: "", page: 1, search: "" });
        setPetitionPrices(response.data.result);
        break;
      case GetData.information:
        response = await getInformationList({
          information: "",
          page: 1,
          search: "",
        });
        setInformations(response.data.result);
        break;
      case GetData.department:
        response = await getDepartmentList({
          department: "",
          page: 1,
          search: "",
        });
        setDepartments(response.data.result);
        break;

      default:
        break;
    }
  };
  const {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    fields,
    control,
    append,
    remove,
    fieldsInformation,
    appendInformation,
    removeInformation,
  } = useFormPetition();

  const submit = async (val: PetitionBody) => {
    let result: any;
    if (defaultValues.id) {
      result = await updatePetition(val, defaultValues.id);
    } else {
      result = await createPetition(val);
    }
    console.log(result);
    showSuccess();
    navigate("/admin/petition");
    setDefaultValues({});
  };

  useEffect(() => {
    getData(GetData.petition_type);
    getData(GetData.petition_price);
    getData(GetData.information);
    getData(GetData.department);

    const getPetition = async (param: GetPetitionParam) => {
      const { data } = await getPetitionList(param);
      setDefaultValues(data.result);
    };
    if (params.id) {
      getPetition({ page: 1, search: "", petition: params.id });
    }
    return () => {
      reset();
    };
  }, [params.id, reset, setDefaultValues]);
  return defaultValues.id || !params.id ? (
    <form onSubmit={handleSubmit(submit)}>
      <div className="grid gap-5">
        <div className="grid grid-cols-3 gap-5">
          <div className="grid col-span-1">
            <b>เลขที่ *</b>
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              {...register("number")}
              required
            />
            <small className=" text-red-500 font-bold">
              {errors.number?.message}{" "}
            </small>
          </div>
          <div className="grid col-span-2">
            <b>ชื่อคำร้อง *</b>
            <input
              type="text"
              className="border border-gray-300 rounded p-2"
              {...register("title")}
              required
            />
            <small className=" text-red-500 font-bold">
              {errors.title?.message}
            </small>
          </div>
        </div>
        <div className="grid grid-cols-3 gap-5">
          <div className=" grid col-span-2">
            <b>ประเภทคำร้อง/วัตถุประสงค์คำร้อง *</b>
            <Controller
              control={control}
              name={`type`}
              rules={{ required: "Type is required" }}
              defaultValue={defaultValues.type}
              render={({ field: { value, onBlur, onChange } }) => (
                <Select
                  options={petitionTypes}
                  getOptionLabel={(option: PetitionTypeBody) =>
                    option.title ? option.title : ""
                  }
                  getOptionValue={(option) =>
                    option.id ? option.id.toString() : ""
                  }
                  onChange={(option) => onChange(option?.id)}
                  onBlur={onBlur}
                  className="w-full"
                  value={petitionTypes.find((d) => d.id === value)}
                />
              )}
            />
            <small className=" text-red-500 font-bold">
              {errors?.type?.message}
            </small>
          </div>
          <div className=" grid">
            <b>ค่าธรรมเนียม</b>
            <Controller
              control={control}
              name="price"
              render={({ field: { value, onBlur, onChange } }) => (
                <Select
                  options={petitionPrices}
                  getOptionLabel={(option: PetitionPriceBody) =>
                    option.price ? option.price : ""
                  }
                  getOptionValue={(option) =>
                    option.id ? option.id.toString() : ""
                  }
                  onChange={(option) => onChange(option?.id)}
                  onBlur={onBlur}
                  className="w-full"
                  value={petitionPrices.find((d) => d.id === value)}
                />
              )}
            />
            <small className=" text-red-500 font-bold">
              {errors?.price?.message}
            </small>
          </div>
        </div>
        <div className="grid">
          <small>สำหรับ</small>
          <div className="pl-2">
            <div className="inline mr-2">
              <input
                {...register("user_level")}
                type="radio"
                name="user_level"
                className="mr-1"
                value={1}
                id="bachelor"
                defaultChecked={defaultValues.user_level === 1}
              />
              <label htmlFor="bachelor">
                <b>นักศึกษา ป.ตรี</b>
              </label>
            </div>
            <div className="inline mr-2">
              <input
                {...register("user_level")}
                type="radio"
                name="user_level"
                className="mr-1"
                value={2}
                id="master"
                defaultChecked={defaultValues.user_level === 2}
              />
              <label htmlFor="master">
                <b>นักศึกษา ป.โท</b>
              </label>
            </div>
            <small className=" text-red-500 font-bold">
              {errors?.user_level?.message}
            </small>
          </div>
        </div>
        <div className="grid">
          <div className="flex justify-between">
            <small>ผู้ดำเนินการ</small>
            <div>
              <button
                className="px-2 font-bold text-yellow-500"
                type="button"
                onClick={() => {
                  for (let i = 0; i < departments.length; i++) {
                    const dept = departments[i];
                    if (dept.id) {
                      append({
                        order: i + 1,
                      });
                      break;
                    }
                  }
                }}
              >
                เพิ่ม
              </button>
            </div>
          </div>
          {fields.map((f, i) => (
            <div className="grid grid-cols-4 gap-5 pl-2" key={f.id + f.order}>
              <div className="grid col-span-1">
                <b>ลำดับ *</b>
                <input
                  type="number"
                  id={`order${i}`}
                  min={1}
                  max={departments.length}
                  className="border border-gray-300 rounded p-2"
                  {...register(`departments.${i}.order`)}
                  required
                />
                <small className=" text-red-500 font-bold">
                  {errors?.departments?.[i].order?.message}
                </small>
              </div>
              <div className="grid col-span-3">
                <b>หน่วยงาน *</b>
                <div className=" flex">
                  <Controller
                    control={control}
                    name={`departments.${i}.id`}
                    rules={{ required: "Department is required" }}
                    render={({
                      field: { value, onBlur, onChange, ref, name },
                    }) => (
                      <Select
                        name={name}
                        ref={ref}
                        options={departments}
                        getOptionLabel={(option) =>
                          option.description ? option.description : ""
                        }
                        getOptionValue={(option) =>
                          option?.id ? option.id.toString() : ""
                        }
                        onChange={(option) => {
                          onChange(option?.id);
                        }}
                        onBlur={onBlur}
                        className="w-full"
                        value={departments.find((d) => d.id === value)}
                      />
                    )}
                  />
                  <button
                    className="px-2 ml-1 rounded bg-gray-200"
                    onClick={() => remove(i)}
                  >
                    -
                  </button>
                </div>
                <small className=" text-red-500 font-bold">
                  {errors?.departments?.[i].id?.message}
                </small>
              </div>
            </div>
          ))}
        </div>
        <div className="grid">
          <div className="flex justify-between">
            <small>ข้อมูลที่ต้องการ</small>
            <div>
              <button
                className="px-2 font-bold text-yellow-500"
                type="button"
                onClick={() => {
                  for (let i = 0; i < informations.length; i++) {
                    const dep = informations[i];
                    if (dep.id) {
                      appendInformation({});
                      break;
                    }
                  }
                }}
              >
                เพิ่ม
              </button>
            </div>
          </div>
          {fieldsInformation.map((f, i) => (
            <div className="grid pl-2" key={f.id}>
              <div className="grid">
                <b>ข้อมูล *</b>
                <div className=" flex">
                  <Controller
                    control={control}
                    name={`informations.${i}.id`}
                    rules={{ required: "Information is required" }}
                    render={({ field: { value, onBlur, onChange } }) => {
                      console.log(value);
                      return (
                        <Select
                          options={informations}
                          getOptionLabel={(option) =>
                            option.title ? option.title : ""
                          }
                          getOptionValue={(option) =>
                            option.id ? option.id.toString() : ""
                          }
                          onChange={(option) => {
                            onChange(option?.id);
                          }}
                          onBlur={onBlur}
                          className="w-full"
                          value={informations.find((d) => d.id === value)}
                        />
                      );
                    }}
                  />
                  <button
                    className="px-2 ml-1 rounded bg-gray-200"
                    onClick={() => removeInformation(i)}
                  >
                    -
                  </button>
                </div>
                <small className=" text-red-500 font-bold">
                  {errors?.informations?.[i]?.id?.message}
                </small>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="text-right mt-3">
        <button
          className="py-2 px-3 text-white bg-yellow-600 rounded"
          type="submit"
        >
          เพิ่ม
        </button>
      </div>
    </form>
  ) : (
    <>Loading...</>
  );
};

export default PetitionForm;
