import { useFieldArray, useForm } from "react-hook-form";

import { useEffect, useState } from "react";
import { PetitionBody } from "../../../hooks/api/types/dataBody";
export const useFormPetition = () => {
  const [defaultValues, setDefaultValues] = useState<PetitionBody>({});
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    control,
  } = useForm<PetitionBody>({
    defaultValues,
  });

  const { fields, append, remove } = useFieldArray({
    name: "departments",
    control,
  });
  const {
    fields: fieldsInformation,
    append: appendInformation,
    remove: removeInformation,
  } = useFieldArray({
    name: "informations",
    control,
  });

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return {
    register,
    handleSubmit,
    reset,
    errors,
    setDefaultValues,
    defaultValues,
    fields,
    append,
    remove,
    control,
    fieldsInformation,
    appendInformation,
    removeInformation,
  };
};
