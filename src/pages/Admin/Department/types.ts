export type DepartmentProps = {
  id?: number;
  description?: string;
  keyword?: string;
};
