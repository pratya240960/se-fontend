import React, { useEffect, useState } from "react";
import Dialog from "../../../components/modal";
import { GetOrderParam } from "../../../hooks/api/types/getParams";
import { getOrderList } from "../../../hooks/api/order";
import { OrderResponse } from "../../Student/Order/types";
import { GetStatusOrder, OrderContentDialog } from "../../../components/Order";

const AdminHomePage: React.FC = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [orders, setOrders] = useState<OrderResponse[]>([]);
  const [order, setOrder] = useState<OrderResponse>();
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const getOrders = async (param: GetOrderParam) => {
    const { data } = await getOrderList(param);
    if (data.result) {
      setOrders(data.result);
    }
  };
  useEffect(() => {
    getOrders({
      order: "",
      page,
      search,
    });
    return () => {};
  }, [page, search]);

  return (
    <div className=" overflow-x-scroll">
      <small className=" text-red-500">*กดที่สถานะเพื่อตรวจสอบ/ดำเนินการ</small>
      <table
        className="w-full text-left border-collapse border"
        style={{ minWidth: "900px" }}
      >
        <thead className="border-bottom py-10">
          <tr>
            <th className="border p-2">คำร้อง</th>
            <th className="border p-2">รหัสนักศึกษา</th>
            <th className="border p-2">ชื่อนักศึกษา</th>
            <th className="border p-2 text-center">สถานะ</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order.id} className="hover:bg-gray-100">
              <td className="border p-2">{order.title} </td>
              <td className="border p-2">{order.actor_id}</td>
              <td className="border p-2">{order.actor_name} </td>
              <td className="border p-2 text-center">
                <GetStatusOrder
                  status={order.status}
                  statusClick={async () => {
                    setIsOpen(true);
                    const { data } = await getOrderList({
                      order: order.id,
                      page: 1,
                      search: "",
                    });
                    setOrder(data.result);
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <IsContentDialog
        open={modalIsOpen}
        setIsOpen={(val) => setIsOpen(val)}
        order={order}
      />
    </div>
  );
};

export default AdminHomePage;

const IsContentDialog: React.FC<{
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  open: boolean;
  order?: OrderResponse;
}> = ({ setIsOpen, open, order }) => {
  return (
    <Dialog open={open} setOpen={setIsOpen}>
      {order ? (
        <OrderContentDialog order={order} onReload={() => setIsOpen(false)} />
      ) : (
        <>Loading</>
      )}
    </Dialog>
  );
};
