import React from "react";
import Modal from "react-modal";
Modal.setAppElement("#root");

const customStyles: Modal.Styles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    borderRadius: "10px",
  },
  overlay: {
    backgroundColor: "rgb(0, 0, 0, 0.3)",
  },
};
interface ModelProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  children: JSX.Element | JSX.Element[] | string | string[];
}
const Dialog: React.FC<ModelProps> = ({
  open,
  setOpen,
  children,
}: ModelProps) => {
  function afterOpenModal() {
    console.log("afterOpenModal");
  }

  function closeModal() {
    setOpen(false);
  }
  return (
    <div>
      <Modal
        isOpen={open}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <div className="pt-5">{children}</div>
      </Modal>
    </div>
  );
};

export default Dialog;
