import { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { updateOrderStatus } from "../../hooks/api/order";
import { DepartmentOperateBody } from "../../hooks/api/types/dataBody";
import { showSuccess } from "../../hooks/sweetAlert";
import { OrderResponse } from "../../pages/Student/Order/types";
import { RootState } from "../../store";

export enum OrderStatus {
  Pending = 1,
  Progress = 2,
  Delivery = 3,
  Success = 4,
  Cancel = 5,
}

export const GetStatusOrder: React.FC<{
  status: OrderStatus;
  statusClick: (text: number) => void;
}> = ({
  status,
  statusClick,
}: {
  status: OrderStatus;
  statusClick: (text: number) => void;
}) => {
  switch (status) {
    case OrderStatus.Pending:
      return (
        <button
          className="text-xs bg-gray-400 rounded-full px-2 p-1"
          onClick={() => statusClick(OrderStatus.Pending)}
        >
          รอการยืนยัน
        </button>
      );
    case OrderStatus.Progress:
      return (
        <button
          className="text-xs bg-yellow-400 rounded-full px-2 p-1"
          onClick={() => statusClick(OrderStatus.Progress)}
        >
          กำลังดำเนินการ
        </button>
      );
    case OrderStatus.Delivery:
      return (
        <button
          className="text-xs bg-blue-400 rounded-full px-2 p-1"
          onClick={() => statusClick(OrderStatus.Delivery)}
        >
          รอการจัดส่ง
        </button>
      );
    case OrderStatus.Success:
      return (
        <button
          className="text-xs bg-green-400 rounded-full px-2 p-1"
          onClick={() => statusClick(OrderStatus.Success)}
        >
          สำเร็จ
        </button>
      );
    case OrderStatus.Cancel:
      return (
        <button
          className="text-xs bg-red-600 text-white rounded-full px-2 p-1"
          onClick={() => statusClick(OrderStatus.Cancel)}
        >
          ไม่สำเร็จ
        </button>
      );

    default:
      return <span>Some ting when wrong!</span>;
  }
};
const OrderStaper: React.FC<{ d: DepartmentOperateBody }> = ({
  d,
}: {
  d: DepartmentOperateBody;
}) => {
  switch (d.isSuccess) {
    case true:
      return (
        <div
          className=" flex flex-col justify-center items-center"
          key={d.department}
        >
          <div className=" bg-green-500 w-10 h-10 rounded-full text-white flex items-center justify-center">
            {d.number}
          </div>
          <small className=" font-bold">{d.title}</small>
          <small className="text-green-500">อนุมัติแล้ว</small>
        </div>
      );
    case false:
      return (
        <div
          className=" flex flex-col justify-center items-center"
          key={d.department}
        >
          <div className=" bg-red-800 w-10 h-10 rounded-full text-white flex items-center justify-center">
            {d.number}
          </div>
          <small className=" font-bold">{d.title}</small>
          <small className="text-red-500">ไม่อนุมัติ</small>
        </div>
      );
    default:
      return (
        <div
          className=" flex flex-col justify-center items-center"
          key={d.department}
        >
          <div className=" bg-gray-200 w-10 h-10 rounded-full text-gray-600 flex items-center justify-center">
            {d.number}
          </div>
          <small className=" font-bold">{d.title}</small>
          <small className="text-gray-500">ยังไม่ดำเนินการ</small>
        </div>
      );
  }
};
export const OrderContentDialog: React.FC<{
  order: OrderResponse;
  onReload: (isReload: boolean) => void;
}> = ({
  order,
  onReload,
}: {
  order: OrderResponse;
  onReload: (isReload: boolean) => void;
}) => {
  const [description, setDescription] = useState<string>("");
  const { user } = useSelector((state: RootState) => state.authen);
  const updateStatus = async (approve: boolean) => {
    await updateOrderStatus({ description, approve }, order.id);
    showSuccess();
    onReload(true);
  };
  return (
    <div className="text-sm">
      <div className="text-center">
        <b className=" text-lg">{order.title}</b>
      </div>
      {user.role !== 2 && (
        <div className="flex justify-around items-center py-7">
          {order.department_operator.map((d) => (
            <OrderStaper d={d} />
          ))}
        </div>
      )}
      <table className="text-left w-full">
        <tbody>
          <tr>
            <th className="py-1 w-32">วันที่ยื่นคำร้อง</th>
            <td>{new Date(order.created_at).toLocaleString("th-TH")}</td>
          </tr>
          <tr>
            <th className="py-1 w-32">หมายเลขคำร้อง</th>
            <td>{order.id}</td>
          </tr>
          <tr>
            <th className="py-1 w-32">คำร้อง</th>
            <td>{order.petition}</td>
          </tr>
          <tr>
            <th className="py-1 w-32">นักศึกษา</th>
            <td>{order.actor_name}</td>
          </tr>
          <tr>
            <th className="py-1 w-32">ข้อมูลการจัดส่ง</th>
            <td>{order.delivery === 1 ? "อีเมล" : "บ้าน"}</td>
          </tr>
          <tr>
            <td>
              <b className="py-1 w-32">ข้อมูล</b>
              <div className="pl-2">
                {order.informations.map((oi) => (
                  <div key={oi.id} className="text-xs">
                    <b>{oi.title} </b>
                    {oi.type === 1 ? (
                      <span> {oi.result}</span>
                    ) : (
                      <a
                        href={oi.result}
                        className=" text-blue-500 hover:underline"
                      >
                        คลิกเพื่อดู
                      </a>
                    )}
                  </div>
                ))}
              </div>
            </td>
          </tr>
          {!order.description && user.role && user.role === 2 ? (
            <tr>
              <td colSpan={2} className="pt-3">
                <div>
                  <b>ข้อคิดเห็น</b>
                  <textarea
                    name="memo"
                    className="w-full bg-gray-100 rounded"
                    rows={3}
                    onChange={(e) => setDescription(e.target.value)}
                  ></textarea>
                </div>
              </td>
            </tr>
          ) : (
            <tr>
              <th className="py-1 w-32">รายละเอียด</th>
              <td>{order.description}</td>
            </tr>
          )}
        </tbody>
      </table>
      {user.role && user.role === 2 && order.status !== 5 ? (
        <div className="flex items-center justify-end pt-5 rounded-b">
          <button
            className="px-3 py-1 rounded text-red-500"
            type="button"
            onClick={() => updateStatus(false)}
          >
            ไม่เห็นชอบ
          </button>
          <button
            className="bg-blue-500 px-3 py-1 rounded text-white"
            type="button"
            onClick={() => updateStatus(true)}
          >
            เห็นชอบ
          </button>
        </div>
      ) : order.status === 5 && user.role && user.role === 1 ? (
        <Link
          to={"/student/order"}
          className="bg-yellow-500 px-3 py-1 rounded text-white mt-5"
          type="button"
        >
          ยื่นคำร้องใหม่
        </Link>
      ) : (
        <button
          className="bg-red-500 px-3 py-1 rounded text-white mt-5"
          type="button"
        >
          ปิด
        </button>
      )}
    </div>
  );
};
